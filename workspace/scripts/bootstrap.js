define(function(require) {
    'use strict';

    require('uiBootstrap');
    require('uiRouter');

    var angular = require('angular');
    var dependencies = [
        // Modules
        require('modules/models/channel'),
        require('modules/utility'),
        require('modules/config/appSettings'),

        // Screens
        require('screens/landing/main'),
        require('screens/featured/main')
    ].map(function(dep) {
            return dep.name;
        });

    var app = angular.module('app', dependencies);
    app.config(require('modules/config/appConfig'));

    angular.element(document).ready(function() {
        angular.element(document.body).removeClass('g-preloader');
        angular.bootstrap(document, [
            'ui.bootstrap',
            'ui.router',
            'app'
        ]);
    });

    console.info('DEBUG. Application loaded in: ', (new Date()) - debugAppStartTime, 'ms');
});