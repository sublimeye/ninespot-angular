define(function() {
    'use strict';

    return ['$urlRouterProvider', '$locationProvider', 'appSettingsProvider',
        function($urlRouterProvider, $locationProvider, appSettingsProvider) {

            $urlRouterProvider.otherwise('/');

            /* Removing #hashbang from URLs; Enabling History API manipulation */
            /* Note: server should rewrite URL to /index.html for all non-API, non-static-file routes */
            $locationProvider.html5Mode(true);
            
            // Init api url
            appSettingsProvider.init("/mobile-api");
            
        }
    ];
});