define(function (require) {
    'use strict';

    var angular = require('angular');
    var module = angular.module('featuredScreen', ['channelModule']);

    module.config(['$stateProvider',
        function ($stateProvider) {

            $stateProvider
                .state('featured', {
                    url: '/featured',
                    views: {
                        'header': {
                            template: require('text!../landing/templates/landingHeader.html')
                        },
                        '': {
                            controller: function($scope, channelService) {
                                channelService.searchChannels({featured:true, count:6}).then(
                                    function(data) {
                                        console.log(data);
                                    },
                                    function(error) {
                                        console.log(error);
                                    });
                            },
                            template: require('text!./templates/featuredContent.html')
                        }
                    }
                });
        }
    ]);
    
    module.service('featuredService', function() {
        
    });
    
    return module;
});
